import os
import logging
import time
import unicodedata
import graphyte


from selenium import webdriver
from bs4 import BeautifulSoup

logging.getLogger().setLevel(logging.INFO)

N_COMPANIES = 4

translation_map = {
    'Акрон': 'Akron',
    'Абрау-Дюрсо': 'Abrau-Dyurso'
}

def extract_metric(page):
    stocks_page = page.findAll('table', {'class': 'tablest'})[0].tbody

    stocks = []
    maxs = []
    mins = []

    blocks = stocks_page.findAll('tr')

    for b in blocks[:N_COMPANIES]:
        items = b.findAll('td')
        name = items[0].a.text
        if name in translation_map:
            name = translation_map[name]
        value = float(items[2].b.text)
        mx = float(items[3].text)
        mi = float(items[4].text)
        name.replace(' ', '_')
        logging.info(name)
        logging.info('{}'.format(value))
        logging.info('{}'.format(mx))
        logging.info('{}'.format(mi))
        stocks.append((name, value))
        maxs.append((name, mx))
        mins.append((name, mi))

    return (stocks, maxs, mins)


GRAPHITE_HOST = 'graphite'

def send_metrics(stocks, prefix):
    sender = graphyte.Sender(GRAPHITE_HOST, prefix=prefix)
    for stock in stocks:
        sender.send(stock[0], stock[1])

def main():

    driver = webdriver.Remote(
        command_executor='http://selenium:4444/wd/hub',
        desired_capabilities={'browserName': 'chrome', 'javascriptEnabled': True}
    )

    driver.get('http://tikr.ru/stock')
    time.sleep(5)
    html = driver.page_source
    soup = BeautifulSoup(html, 'html.parser')

    stocks, maxs, mins = extract_metric(soup)
    send_metrics(stocks, 'stocks')
    send_metrics(maxs, 'stocks_max')
    send_metrics(mins, 'stocks_min')

    logging.info('Page title: %s', driver.title)
    
    driver.quit()


if __name__ == '__main__':
    main()
